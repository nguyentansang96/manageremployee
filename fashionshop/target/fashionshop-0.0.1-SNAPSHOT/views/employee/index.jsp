<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Manager Employee</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
  
  <h2>Add New Employee</h2>
    <div class="col-md-6">
      <form action="/fashionshop/manager/index" method="POST">
        <div class="form-group">
          <label for="name">Name:</label>
          <input type="name" class="form-control" id="name" placeholder="Enter name">
          <span id="error_name" style="display:none">*name is not empty</span>
        </div>
        <div class="form-group">
          <label for="age">Age:</label>
          <input type="text" class="form-control" id="age" placeholder="Enter age">
          <span id="error_age" style="display:none">*age is not empty</span>
        </div>
        <button type="button" id="btn_add" class="btn btn-warning">Add</button>
      </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="<c:url value='/resources/js/employee.js' />"></script>
</body>
</html>