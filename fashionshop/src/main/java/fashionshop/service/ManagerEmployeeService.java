package fashionshop.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fashionshop.dao.ManagerEmployeeDao;
import fashionshop.model.Employee;

@Service
@Transactional
public class ManagerEmployeeService {
	@Autowired
	private ManagerEmployeeDao managerEmployeeDao;
	/*
	 * public void save(NhanVien nhanVien) { managerEmployeeDao.save(nhanVien); }
	 */
	public List<Employee> getAllEmployee(){
		return managerEmployeeDao.getAllEmployee();
	}
	public void addEmployee(Employee employee) throws SQLException {
		managerEmployeeDao.addEmployee(employee);
	}
	public List<Employee> findEmployee(String email){
		return managerEmployeeDao.findEmployee(email);
	}
	public void deleteEmployee(int id) {
		managerEmployeeDao.deleteEmployee(id);
	}
}
