package fashionshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fashionshop.model.Person;

@Controller
@RequestMapping("/home")
public class AjaxController {
	private List<Person> listPerson = new ArrayList<Person>();

	@RequestMapping(value="/add",method = RequestMethod.GET)
	public String home() {
		return "home";
	}
	@RequestMapping(value="/test",method = RequestMethod.GET)
	public ModelAndView test() {
		return new ModelAndView("hello");
	}
	@RequestMapping("/add-new")
	public @ResponseBody String addNew(HttpServletRequest  request) {
		String name = request.getParameter("name");
		String age = request.getParameter("age");

//		String name = "sang";
//		String age = "24";

		Person person = new Person(name, Integer.parseInt(age));
		listPerson.add(person);

		ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			ajaxResponse = mapper.writeValueAsString(person);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ajaxResponse;
	}
}
